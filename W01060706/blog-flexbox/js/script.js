function inicio(){
    var calendario = document.getElementById("tabla-calendario")
    var dia = 0;
    var is_julio = false;
    for (let index = 0; index < 5; index++) {
        
        var fila = document.createElement('tr');
        for (let index1 = 0; index1 < 7; index1++) {
            dia++;
            var elemento = document.createElement('td');
            elemento.textContent = dia;
            
            if(is_julio){
                elemento.className = "julio";
            }
            if(dia >= 30){
                dia = 0;
                is_julio = true;
            }
            if(dia == new Date().getDate() && !is_julio){
                elemento.id ="hoy";
            }

            fila.appendChild(elemento);
        }
        calendario.appendChild(fila);
    }
}