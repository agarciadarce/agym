let ancho = 0;
let largo = 0;
var figura;

function cargar() {
    ancho = document.getElementById("figura").clientWidth;
    alto = document.getElementById("figura").clientHeight;
    figura = document.getElementById("figura");
}

function posicionX() {
    var element = document.getElementById("posx");
    figura.style.left = element.value + "px";
}

function posicionY() {
    var element = document.getElementById("posy");
    figura.style.top = element.value + "px";
}

function tam() {
    var element = document.getElementById("tam");
    figura.style.width = (ancho * element.value) + "px";
    figura.style.height = (alto * element.value) + "px";
}

function opacidad() {
    var element = document.getElementById("opacidad");
    figura.style.opacity = element.value;
}

function forma() {
    var element = document.getElementById("forma");
    switch (element.value) {
        case "cuadrado":
            figura.style.transform = "rotate(0deg)";
            figura.style.borderRadius = "0%";
            break;
        case "circulo":
            figura.style.borderRadius = "50%";
            break;
        case "rombo":
            figura.style.borderRadius = "0%";
            figura.style.transform = "rotate(45deg)";
            isRombo = true;
            break;

        default:
            alert('CORRAN');
            break;
    }
}

function colorHEX(event) {

    if (event.code == "Enter") {
        var element = document.getElementById("colorHEX");

        figura.style.backgroundColor = "#" + element.value;
    }
}

function rgba() {
    var R = document.getElementById("r").value;
    var G = document.getElementById("g").value;
    var B = document.getElementById("b").value;
    var A = document.getElementById("a").value;

    figura.style.backgroundColor = `rgba(${R},${G},${B},${A})`;

}